const { auth } = require("google-auth-library");
const keys = require("../authKeys/admanagerAuthKeys");
const { DFP } = require("google-ad-manager-api");
const dfp = new DFP({ networkCode: "16123186", apiVersion: "v202008" });
const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
  async function dfpquery() {
    try {
      const client = auth.fromJSON(keys);
      client.scopes = ["https://www.googleapis.com/auth/dfp"];
      await client.authorize();
      const lineItemService = await dfp.getService("LineItemService");
      lineItemService.setToken(client.credentials.access_token);

      const lineItem = await lineItemService.getLineItemsByStatement({
        filterStatement: {
          query: "WHERE status = 'DELIVERING'"
        }
      });

      if (!lineItem)
        return res
          .status(404)
          .send("The line item with the given ID was not found");
      console.log(lineItem);
      lineItem.results.forEach(function(value) {
        const lineItemInfo =
          "Line item id: <a href=" +
          value.id + ">" + value.id +
          "</a>. Line item name: " +
          value.name +
          ". Start: " +
          value.startDateTime +
          "\n";
        console.log(value.id, value.name);
        res.write(lineItemInfo);
      });
      res.end();
    } catch (err) {
      console.log("Error", err);
    }
  }
  dfpquery();
});

router.get("/:id", (req, res) => {
  async function dfpquery() {
    try {
      const client = auth.fromJSON(keys);
      client.scopes = ["https://www.googleapis.com/auth/dfp"];
      await client.authorize();
      const lineItemService = await dfp.getService("LineItemService");
      lineItemService.setToken(client.credentials.access_token);
      const queryId = req.params.id;
      const lineItem = await lineItemService.getLineItemsByStatement({
        filterStatement: {
          query: "WHERE id = '" + queryId + "'"
        }
      });

      if (!lineItem)
        return res
          .status(404)
          .send("The line item with the given ID was not found");
      console.log(lineItem);
      res.send("Order id: " + lineItem.results[0].orderId + "<br>" +
      "Order Name: " + lineItem.results[0].orderName + "<br>" +
      "Line Item id: " + lineItem.results[0].id + "<br>" +
      "Lline Item Name: " + lineItem.results[0].name + "<br>" +
      "Type: <span style=\"text-transform: capitalize;\">" + lineItem.results[0].lineItemType.toLowerCase() + "</span><br>" + 
      "Status: <span style=\"text-transform: capitalize;\">" + lineItem.results[0].status.toLowerCase() + "</span><br>" +
      "Notes: " + lineItem.results[0].notes + "<br>");
    } catch (err) {
      console.log("Error", err);
    }
  }
  dfpquery();
});

module.exports = router;
