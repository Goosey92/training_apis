const { auth } = require("google-auth-library");
const keys = require("../authKeys/admanagerAuthKeys");
const { DFP } = require("google-ad-manager-api");
const dfp = new DFP({ networkCode: "5574", apiVersion: "v202105" });
const express = require("express");
const fs = require("fs");
const router = express.Router();

router.get("/", (req, res) => {
    async function dfpquery() {
        try {
            var addToObject = function (obj, key, value, index) {
                var temp = {};
                var i = 0;
                for (var prop in obj) {
                    if (obj.hasOwnProperty(prop)) {
                        if (i === index && key && value) {
                            temp[key] = value;
                        }
                        temp[prop] = obj[prop];
                        i++;
                    }
                }
                if (!index && key && value) {
                    temp[key] = value;
                }
                return temp;
            };
            const client = auth.fromJSON(keys);
            client.scopes = ["https://www.googleapis.com/auth/dfp"];
            await client.authorize();
            const inventoryService = await dfp.getService("InventoryService");
            inventoryService.setToken(client.credentials.access_token);
            var idString = fs.readFileSync('./AdUnitIds/NativeAdUnits.csv', 'utf8');
            var allAdUnitID = idString.split(',');
            for (var i = 0; i < allAdUnitID.length; i++) {
                console.log("Running process: " + i + " out of: " + allAdUnitID.length)
                var rewrite = idString.replace(new RegExp(allAdUnitID[i].toString()+","), '');
                fs.writeFileSync('./AdUnitIds/NativeAdUnits.csv', rewrite, 'utf-8');
                var currentID = allAdUnitID[i];
                //console.log("Current ID: " + currentID);
                var adUnit = await inventoryService.getAdUnitsByStatement({
                    filterStatement: {
                        query: "WHERE id = '" + currentID + "'"
                    }
                });
                if (adUnit.results == 'undefined' || adUnit.totalResultSetSize == 0) {
                    //console.log("Skipped ID: " + currentID);
                }
                else {
                    var adUnitresult = adUnit;
                    //console.log(adUnitresult)
                    try {
                        
                        var oldSize = adUnitresult.results[0].adUnitSizes[0];
                    }
                    catch {
                        var oldSize = {
                            size: { width: 320, height: 100, isAspectRatio: false },
                            environmentType: 'BROWSER',
                                fullDisplayString: '320x100',
                                    isAudio: false
                        }

                    }
                    var newSize = Object.assign({}, oldSize);
                    newSize.size = Object.assign({}, oldSize.size);
                    newSize.fullDisplayString = '320x100';
                    newSize.size.height = '100';
                    newSize.size.width = '320';
                    try {
                        adUnitresult.results[0].adUnitSizes.push(newSize);
                    }
                    catch {
                        adUnitresult.results[0] = addToObject(adUnitresult.results[0], 'adUnitSizes', [], 9);
                        adUnitresult.results[0].adUnitSizes.push(newSize);
                        //console.log(adUnitresult)
                    }
                    const updateAds = await dfp.getService("InventoryService");
                    updateAds.setToken(client.credentials.access_token);

                    const inventory = await updateAds.updateAdUnits({
                        'adUnits': adUnitresult.results[0]
                    });

                    console.log("New number of size arrays for ad unit ID:" + currentID + " NAME: " + inventory[0].name + ": " + inventory[0].adUnitSizes.length)
                }
            }
            res.end();
        } catch (err) {
            console.log("Error", err);
        }
    }
    dfpquery();
});

module.exports = router;