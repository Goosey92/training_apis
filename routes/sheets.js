const {
        auth
    } = require("google-auth-library");
const fs = require('fs');
const readline = require('readline');
const {
        google
    } = require('googleapis');
const express = require("express");
const router = express.Router();
router.get("/", (req, res) => {
        async function sheetQuery() {
            try {
                debugger;
                const SCOPES = ['https://www.googleapis.com/auth/spreadsheets', 'https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive'];
                const TOKEN_PATH = 'token.json';
                fs.readFile('./authKeys/credentials.json', (err, content) => {
                    if (err) return console.log('Error loading client secret file:', err);
                    authorize(JSON.parse(content), createSpreadSheet);
                });
                function authorize(credentials, callback) {
                    const { client_secret, client_id, redirect_uris } = credentials.installed;
                    const oAuth2Client = new google.auth.OAuth2(
                        client_id, client_secret, redirect_uris[0]);
                    // Check if we have previously stored a token.
                    fs.readFile(TOKEN_PATH, (err, token) => {
                        if (err) return getNewToken(oAuth2Client, callback);
                        oAuth2Client.setCredentials(JSON.parse(token));
                        callback(oAuth2Client);
                    });
                }
                function getNewToken(oAuth2Client, callback) {
                    const authUrl = oAuth2Client.generateAuthUrl({
                        access_type: 'offline',
                        scope: SCOPES,
                    });
                    console.log('Authorize this app by visiting this url:', authUrl);
                    const rl = readline.createInterface({
                        input: process.stdin,
                        output: process.stdout,
                    });
                    rl.question('Enter the code from that page here: ', (code) => {
                        rl.close();
                        oAuth2Client.getToken(code, (err, token) => {
                            if (err) return console.error('Error while trying to retrieve access token', err);
                            oAuth2Client.setCredentials(token);
                            // Store the token to disk for later program executions
                            fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                                if (err) return console.error(err);
                                console.log('Token stored to', TOKEN_PATH);
                            });
                            callback(oAuth2Client);
                        });
                    });
                }
                async function createSpreadSheet(auth) {
                    debugger;
                    const sheets = google.sheets({ version: 'v4', auth });
                    var files = fs.readdirSync('./Downloads/');
                    const xlsx = require("xlsx");
                    var i;
                    var file;
                    for (i = 0; i < files.length; i++) {
                        file += files[i];
                        if (file.includes("undefined")) {
                            file = file.replace("undefined", "");
                        }
                        const workBook = xlsx.readFile("./Downloads/" + file);
                        const sheet_Name_List = workBook.SheetNames;
                        var xlsxFile = xlsx.utils.sheet_to_json(workBook.Sheets[sheet_Name_List[0]])
                        try {
                            const request = {
                                resource: {
                                    properties: {
                                        title: file
                                    },
                                },
                                auth: auth,
                            };
                            try {
                                const response = (await sheets.spreadsheets.create(request)).data;
                                console.log(JSON.stringify(response, null, 2));
                                const titles = ['Order', 'Line Item', 'Creative Size (delivered)', 'Order ID',
                                    'Line item ID', 'Line item start date', 'Line item end date', 'Ad server impressions', 'Ad server clicks', 'Ad server CTR']
                                const Appendrequest = {
                                    spreadsheetId: response.spreadsheetId,                                
                                    insertDataOption: 'OVERWRITE', 
                                    resource: {
                                        "values": [
                                            titles
                                        ]
                                    },

                                    auth: auth,
                                };
                                try {
                                    const response = (await sheets.spreadsheets.values.append(Appendrequest)).data;
                                    // TODO: Change code below to process the `response` object:
                                    console.log(JSON.stringify(response, null, 2));
                                    const Bulkrequest = {
                                        spreadsheetId: response.spreadsheetId,
                                        resource: {
                                            valueInputOption: 'USER_ENTERED',  // TODO: Update placeholder value.
                                            // The new values to apply to the spreadsheet.
                                            data: [
                                                {

                                                    "values": xlsxFile

                                                }

                                            ],  // TODO: Update placeholder value.
                                            // TODO: Add desired properties to the request body.
                                        },
                                        auth: auth,
                                    };
                                    try {
                                        const response = (await sheets.spreadsheets.values.append(Bulkrequest)).data;
                                        // TODO: Change code below to process the `response` object:
                                        console.log(JSON.stringify(response, null, 2));
                                        file = "";
                                    } catch (err) {
                                        console.error(err);
                                        file = "";
                                    }
                                } catch (err) {
                                    console.error(err);
                                    file = "";
                                }
                            } catch (err) {
                                console.error(err);
                                file = "";
                            }

                        } catch (err) {
                            console.error(err);
                        }
                    }

                }
            }
            catch (e) {
                console.log(e);
            }
        }
    sheetQuery();
});
module.exports = router;