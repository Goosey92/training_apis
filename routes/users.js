const { auth } = require("google-auth-library");
const keys = require("../authKeys/admanagerAuthKeys");
const { DFP } = require("google-ad-manager-api");
const dfp = new DFP({ networkCode: "16123186", apiVersion: "v202008"});
const express = require("express");
const { CLOUD_SDK_CLIENT_ID } = require("google-auth-library/build/src/auth/googleauth");
const router = express.Router();

router.get("/", (req, res) => {
    async function dfpquery() {
        try {
            const client = auth.fromJSON(keys);
            client.scopes = ["https://www.googleapis.com/auth/dfp"];
            await client.authorize();
            const userService = await dfp.getService('UserService', client.credentials.access_token);
            
            const activeUsers = await userService.getUsersByStatement({
                filterStatement: {
                    query: "WHERE status = 'ACTIVE' OR status = 'INACTIVE'"
                }
            });

            if(!activeUsers)
            return res
            .status(404)
            .send("Can't find any users");
            console.log(activeUsers);

            activeUsers.results.forEach(function(value) {
                const userInfo = 
                "ID: " + value.id +
                " Username: " +
                value.name +
                " Role " +
                value.roleName +
                " Active: " +
                value.isActive +
                "\n";
                res.write(userInfo);
            });

            res.end();
        } catch (err) {
            console.log("You fucked up", err);
        }
    }
    dfpquery();
});

module.exports = router;