const { auth } = require("google-auth-library");
const keys = require("../authKeys/admanagerAuthKeys");
const { DFP } = require("google-ad-manager-api");
const dfp = new DFP({ networkCode: "5574", apiVersion: "v202111" });
const express = require("express");
const router = express.Router();
const fs = require("fs");
router.get("/", (req, res) => {
  async function dfpquery() {
      try {
            const client = auth.fromJSON(keys);
            client.scopes = ["https://www.googleapis.com/auth/dfp"];
            await client.authorize();
            const inventoryService = await dfp.getService("InventoryService");
          inventoryService.setToken(client.credentials.access_token);
          debugger;
            const adUnit = await inventoryService.getAdUnitsByStatement({
                filterStatement: {
                    //query: "WHERE status = 'ACTIVE' AND id >= '22234319353' AND id <= '22235319353'"
                          query: "WHERE status = 'ACTIVE' AND id >= '22234761731'"
                }
            });
          debugger;
            if (!adUnit)dex
                return res
                    .status(404)
                    .send("The ad unit was not found");
            var csvArrayHeaders = ["adUnitCode", "adUnitSizes", "id", "name"];
            var CSVStringHeaders = csvArrayHeaders.join(",") + '\n';
          var csvArrayRows = [];
          var addToObject = function (obj, key, value, index) {
              var temp = {};
              var i = 0;
              for (var prop in obj) {
                  if (obj.hasOwnProperty(prop)) {
                      if (i == index) {
                          temp[key] = value;
                      }
                      temp[prop] = obj[prop];
                      i++;
                  }
              }
              return temp;
          };
            console.dir("Success!");
            var size = 8999; var arrayOfArrays = [];
            for (var i = 0; i < adUnit.results.length; i += size) {
                arrayOfArrays.push(adUnit.results.slice(i, i + size));
            }
            var currentRow = '';
          var currentRowArray = [];
          var differentAdUnits = ['isInterstitial','isNative','isFluid']
            for (var i = 0; i < arrayOfArrays.length; i++) {
                for (var f = 0; f < arrayOfArrays[i].length; f++) {
                    if (!Object.keys(arrayOfArrays[i][f]).includes("adUnitSizes")) {
                        arrayOfArrays[i][f] = addToObject(arrayOfArrays[i][f], 'adUnitSizes', '', 9);
                        //Object.assign(arrayOfArrays[i][f], { 'adUnitSizes': "" });
                    }
                    for (const [key, value] of Object.entries(arrayOfArrays[i][f])) {
                        //var index = Object.keys(arrayOfArrays[i][f]).indexOf('adUnitSizes');
                        if (key == 'name' || key == 'id' || key == 'adUnitCode' || key == 'adUnitSizes' || key == 'parentPath' || differentAdUnits.some(substring => key.includes(substring))) {
                            if (key == 'parentPath') {
                                //var highestNumberofArray = arrayOfArrays[i][f].parentPath.length - 1;
                                var highestNumberofArray = arrayOfArrays[i][f].parentPath;
                                var ppi = [];
                                var ppn = [];
                                var ppa = [];
                                for (var x = 0; x < highestNumberofArray.length; x++) {
                                    ppi.push(highestNumberofArray[x].id);
                                    ppn.push(highestNumberofArray[x].name);
                                    ppa.push(highestNumberofArray[x].adUnitCode);
                                }
                                //try {
                                //  var parentPathName = arrayOfArrays[i][f].parentPath[highestNumberofArray].name;
                                //  var parentPathCode = arrayOfArrays[i][f].parentPath[highestNumberofArray].adUnitCode;
                                //}
                                //catch{
                                //    var parentPathName = arrayOfArrays[i][f].parentPath[0].name;
                                //    var parentPathCode = arrayOfArrays[i][f].parentPath[0].adUnitCode;
                                //}

                                currentRow = 'ParentPathName: ' + ppn.join("/") + ", ParentPathAdUnitCode: " + ppa.join("/") + ", ParentIds: " + ppi.join("/") + ", ";
                                var promise = fs.appendFileSync("./downloads/adunits.txt", currentRow, function (err) {
                                    if (err) {
                                        return console.log(err);
                                    }
                                });

                            }
                            else {
                                if (key == 'adUnitSizes') {
                                    
                                    currentRow = 'adUnitSizes: ';
                                    for (var t = 0; t < value.length; t++) {

                                        var currentSize = value[t].fullDisplayString;
                                        currentRow += currentSize + " ";
                                    }
                                    var parentPathSizes = arrayOfArrays[i][f].parentPath || 0;
                                    for (var x = 0; x < parentPathSizes.length; x++) {
                                        if (parentPathSizes[x].name.toString().includes(".native") || arrayOfArrays[i][f].adUnitCode.toString().includes(".native")) {
                                            
                                            currentRow += 'fluid'
                                            break;
                                        }
                                        if (parentPathSizes[x].
                                            name.toString().
                                            includes('outstream')|| parentPathSizes[x].
                                                name.toString().
                                                includes('interstitial') || arrayOfArrays[i][f]
                                                .adUnitCode.toString()
                                                .includes('outstream') || arrayOfArrays[i][f]
                                                    .adUnitCode.toString()
                                                        .includes('interstitial')){
                                            currentRow += 'out of page'
                                            break;
                                        }

                                    }
                                    currentRow += ","
                                    var promise = fs.appendFileSync("./downloads/adunits.txt", currentRow, function (err) {
                                        if (err) {
                                            return console.log(err);
                                        }
                                    });
                                }
                                else {
                                    console.log(`${key}: ${value}`);
                                    if (key == 'isFluid') {
                                        currentRow = `${'isFluid-'}: ${value}`;
                                        //currentRow.replace(/isFluid:/g, 'isFluid')
                                        currentRow + '\n';
                                    }
                                    else {
                                        currentRow = `${key}: ${value}` + ', ';
                                    }
                                    var promise = fs.appendFileSync("./downloads/adunits.txt", currentRow, function (err) {
                                        if (err) {
                                            return console.log(err);
                                        }
                                    });
                                }
                            }
                        }
                        else {

                        }
                    }
                }
            }
          var fileCSV = fs.readFileSync("./downloads/adunits.txt");
          fileCSV = fileCSV.toString('utf8');
          debugger
            fileCSV = fileCSV.replace(/id:/g, ";id:");
            var ar = fileCSV.split(';'); // split string on comma space
            var newArr = [];
            var csvArr = [];
            for (var n = 0; n < ar.length; n++) {
                //var currentLine = ar[n].replace(/id: /g, "").replace(/ParentPathName: /g, "").replace(/name: /g, "").replace(/adUnitCode: /g, "").replace(/adUnitSizes: /g, "").replace(/,/g, "");
                var currentLine = ar[n].replace(/id: /g, "").replace(/ParentPathName: /g, "")
                    .replace(/name: /g, "").replace(/adUnitCode: /g, "")
                    .replace(/adUnitSizes: /g, "").replace(/ParentPathAdUnitCode: /g, "")
                    .replace(/ParentIds: /g, "");
                newArr.push(currentLine.replace(/.$/, "\n"));
            }
            for (var f = 0; f < newArr.length; f++) {
                let text = newArr[f].split('\n')                               // split lines
                    .map(line => line.split(/,/).join(','))  // split spaces then join with ,
                    .join('\n')
                csvArr.push(text);
            }
            for (var g = 0; g < csvArr.length; g++) {
                var promise = fs.appendFileSync("./downloads/adunits.csv", csvArr[g], function (err) {
                    if (err) {
                        return console.log(err);
                    }
                });
            }
      res.end();
    } catch (err) {
      console.log("Error", err);
    }
  }
  dfpquery();
});

router.get("/:id", (req, res) => {
  async function dfpquery() {
    try {
      const client = auth.fromJSON(keys);
      client.scopes = ["https://www.googleapis.com/auth/dfp"];
      await client.authorize();
      const inventoryService = await dfp.getService("InventoryService");
      inventoryService.setToken(client.credentials.access_token);
      const queryId = req.params.id;
      const adUnit = await inventoryService.getAdUnitsByStatement({
        filterStatement: {
          query: "WHERE id = '" + queryId + "'"
        }
      });

      if (!adUnit)
        return res
          .status(404)
          .send("The ad unit with the given ID was not found");
      console.log(adUnit);
      res.send("Ad Unit id: " + adUnit.results[0].id + "<br>" +
      "Ad Unit Name: " + adUnit.results[0].name + "<br>" +
      "Parent id: " + adUnit.results[0].parentId + "<br>" +
      "Ad Unit Sizes: " + adUnit.results[0].adUnitSizes.AdUnitSize + "<br>" +
      "Ad Unit Code: " + adUnit.results[0].adUnitCode + "<br>" + 
      "Status: <span style=\"text-transform: capitalize;\">" + adUnit.results[0].status.toLowerCase() + "</span><br>" +
      "Target Window: " + adUnit.results[0].targetWindow + "<br>");
    } catch (err) {
      console.log("Error", err);
    }
  }
  dfpquery();
});

module.exports = router;
