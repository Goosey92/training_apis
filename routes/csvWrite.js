const { auth } = require("google-auth-library");
const keys = require("../authKeys/admanagerAuthKeys");
const { DFP } = require("google-ad-manager-api");
const dfp = new DFP({ networkCode: "5574", apiVersion: "v202008" });
const express = require("express");
const router = express.Router();
const fs = require("fs");
router.get("/", (req, res) => {
    async function dfpquery() {
        try {
            var fileCSV = fs.readFileSync("./downloads/adunits.txt", "utf-8");
            fileCSV = fileCSV.replace(/id:/g, ";id:");
            var ar = fileCSV.split(';'); // split string on comma space
            var newArr = [];
            var csvArr = [];
            for (var n = 0; n < ar.length; n++) {
                //var currentLine = ar[n].replace(/id: /g, "").replace(/ParentPathName: /g, "").replace(/name: /g, "").replace(/adUnitCode: /g, "").replace(/adUnitSizes: /g, "").replace(/,/g, "");
                var currentLine = ar[n].replace(/id: /g, "").replace(/ParentPathName: /g, "")
                    .replace(/name: /g, "").replace(/adUnitCode: /g, "")
                    .replace(/adUnitSizes: /g, "").replace(/ParentPathAdUnitCode: /g, "")
                    .replace(/ParentIds: /g, "");
                newArr.push(currentLine.replace(/.$/, "\n"));
            }
            for (var f = 0; f < newArr.length; f++) {
                let text = newArr[f].split('\n')                               // split lines
                    .map(line => line.split(/,/).join(','))  // split spaces then join with ,
                    .join('\n')
                csvArr.push(text);
            }
            for (var g = 0; g < csvArr.length; g++) {
                //console.log(csvArr[g]);
                var promise = fs.appendFileSync("./downloads/adunits.csv", csvArr[g], function (err) {
                    if (err) {
                        return console.log(err);
                    }
                });
            }
            res.end();
        } catch (err) {
            console.log("Error", err);
        }
    }
    dfpquery();
});
module.exports = router;
