const { auth } = require("google-auth-library"); 
const keys = require("../authKeys/admanagerAuthKeys");
const { DFP } = require("google-ad-manager-api");
const dfp = new DFP({ networkCode: "154725070", apiVersion: "v202111"});
const express = require("express");
const { CLOUD_SDK_CLIENT_ID } = require("google-auth-library/build/src/auth/googleauth");
const router = express.Router();
//Doms
const {google} = require('googleapis');

const { debug } = require("console");
const { runInNewContext } = require("vm");
let usersDom = [["ID","NAME","ROLE","ACTIVE","EMAIL"]];

//https://docs.google.com/spreadsheets/d/1Bki2U8Ff6XvZClVxm0AvNiyTsTnfSGDgPhnNXkK9BAU/edit#gid=0 this is the sheet where the info is sent 

router.get("/", (req, res) => {
    async function dfpquery() {
        try {
            const client = auth.fromJSON(keys);
            client.scopes = ["https://www.googleapis.com/auth/dfp", "https://www.googleapis.com/auth/spreadsheets"];
            await client.authorize();
            const userService = await dfp.getService('UserService', client.credentials.access_token);
            const gsapi = google.sheets({version: "v4", auth: client});
            const activeUsers = await userService.getUsersByStatement({
                filterStatement: {
                    query: "WHERE status = 'ACTIVE' OR status = 'INACTIVE'"
                }
            });

            if(!activeUsers)
            return res
            .status(404)
            .send("Can't find any users");
            // console.log(activeUsers);
            

            
            activeUsers.results.forEach(function(value) {
                const userInfo = 
                "ID: " + value.id +
                ", Username: " +
                value.name +
                ", Role " +
                value.roleName + 
                ", Active: " +
                value.isActive +
                ", Email: " + 
                value.email +
                "\n";
               //added this to pauls original code to create arrays
                    usersDom.push([value.id, value.name, value.roleName, value.isActive, value.email]);
                
                
            
                res.write(userInfo);
                
                
                
            });

            //sheet update function and credentials 
            const updateSheet = {
                spreadsheetId: '1Bki2U8Ff6XvZClVxm0AvNiyTsTnfSGDgPhnNXkK9BAU',
                range: 'Sheet1!A1',
                valueInputOption: 'USER_ENTERED',
                resource: { values: usersDom}    
            };
            // console.log(usersDom);
            let response = await gsapi.spreadsheets.values.update(updateSheet);        
            console.log(usersDom);

            
            
            res.end();
        } catch (err) {
            console.log("You fucked up", err);
        }
    }
    dfpquery();
});

module.exports = router;


