const {
    auth
} = require("google-auth-library");
const keys = require("../authKeys/admanagerAuthKeys");
const {
    DFP
} = require("google-ad-manager-api");
const dfp = new DFP({
    networkCode: "16123186",
    apiVersion: "v202008"
});
const mv = require("mv");
const path = require("path");
const fs = require("fs")
const express = require("express");
const { CLOUD_SDK_CLIENT_ID } = require("google-auth-library/build/src/auth/googleauth");
const { report } = require("./users");
const router = express.Router();
router.get("/", (req, res) => {
    async function dfpquery() {
        try {
            const client = auth.fromJSON(keys);
            client.scopes = ["https://www.googleapis.com/auth/dfp"];
            await client.authorize();
            const reportService = await dfp.getService("ReportService");
            reportService.setToken(client.credentials.access_token);
            console.log("Getting Definitions");
            //var results = null;
            const args = {
                reportJob: {
                    reportQuery: {
                        dimensions: ['ORDER_NAME', 'LINE_ITEM_NAME', 'CREATIVE_SIZE_DELIVERED'],
                        columns: ['AD_SERVER_IMPRESSIONS', 'AD_SERVER_CLICKS', 'AD_SERVER_CTR'],
                        dimensionAttributes: ['LINE_ITEM_START_DATE_TIME', 'LINE_ITEM_END_DATE_TIME'],
                        dateRangeType: ["LAST_3_MONTHS"],
                        dimensionFilters: [],
                        //statement: { query: 'WHERE LINE_ITEM_ID = 4794452361' }
                        //timeZoneType: ['EUROPE/LONDON']
                    }
                }
            };
            function download_report(reportURL, local_filename) {
                var https = require('https');
                var fs = require('fs');
                var zlib = require('zlib');
                var file = fs.createWriteStream(local_filename);
                https.get(reportURL, function (response) {
                    response.pipe(zlib.createGunzip()).pipe(file);
                });
            }
            const report = await reportService.runReportJob(args, function (err, jobStatus) {
                if (err) {
                    return console.log('ERROR', err.body);
                }
            });
            var reportComplete = false;
            while (reportComplete == false) {
                try {
                    const statusReport = await reportService.getReportJobStatus({ reportJobId: report.id });
                    if (statusReport == 'COMPLETED') {
                        console.log("Report Completed.");
                        reportComplete = true;
                    }
                    else if (statusReport == "FAILED") {
                        console.log("The Report failed!");
                        return ("The report failed to run.");
                    }
                    else {
                        console.log("Report Pending...");
                        reportComplete = false;
                    }
                }
                catch (err) {
                    console.log(err);
                }
            }
            var reportId = report.id;
            const reportURL = await reportService.getReportDownloadUrlWithOptions({
                reportJobId: reportId, reportDownloadOptions: {
                    exportFormat: 'XLSX',
                    includeReportProperties: false,
                    includeTotalsRow: false,
                    useGzipCompression: true
                }  });
            try {
                var fileName = 'downloaded_report_' + reportId + '.XLSX';
                download_report(reportURL, fileName);
                console.log("Report Complete");
                console.log("Moving file to downloads folder.");
                var filePathFull = __dirname;
                var filePath = filePathFull.replace("routes", "");
                const currentPath = path.join(filePath, "", fileName);
                const destinationPath = path.join(filePath, "Downloads", fileName);
                mv(currentPath, destinationPath, function (err) {
                    if (err) {
                        throw err
                    } else {
                        console.log("Successfully moved the file!");
                    }
                });
                console.log("Report complete.");
                res.send("Report Complete!");
            }
            catch (e) {
                console.log(e);

            }

        } catch (error) {
            console.log("Error, " + error);
        }
    }
    console.log("Started report");
    dfpquery();
});
module.exports = router;